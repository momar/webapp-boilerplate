module codeberg.org/momar/webapp-boilerplate

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.1.2
	github.com/lib/pq v1.8.0
	github.com/markbates/pkger v0.17.1
	github.com/rs/zerolog v1.20.0
)
