package parsable

import (
	"errors"
	"github.com/google/uuid"
)

type ParsableUUID struct {
	errorHandler func(string, error)
	name         string
	value        uuid.UUID
	err          error
}

func (p *Parsable) AsUUID() *ParsableUUID {
	if p.value == "" && p.required {
		return &ParsableUUID{p.errorHandler, p.name, uuid.UUID{}, errors.New("field is required")}
	} else if p.value == "" && p.fallback == "" {
		return &ParsableUUID{p.errorHandler, p.name, uuid.UUID{}, nil}
	} else if p.value == "" {
		p.value = p.fallback
	}
	v, err := uuid.Parse(p.value)
	return &ParsableUUID{p.errorHandler, p.name, v, err}
}

func (p *ParsableUUID) Get() uuid.UUID {
	if p.err != nil {
		p.errorHandler(p.name, p.err)
	}
	return p.value
}

func (p *ParsableUUID) Error() error {
	return p.err
}

func (p *ParsableUUID) GenerateIfMissing() *ParsableUUID {
	if p.value == (uuid.UUID{}) {
		p.value = uuid.New()
	}
	return p
}
