package parsable

import (
	"errors"
	"fmt"
	"regexp"
)

type ParsableString struct {
	errorHandler func(string, error)
	name         string
	value        string
	err          error
}

func (p *Parsable) AsString() *ParsableString {
	if p.value == "" && p.required {
		return &ParsableString{p.errorHandler, p.name, "", errors.New("field is required")}
	} else if p.value == "" {
		p.value = p.fallback
	}
	return &ParsableString{p.errorHandler, p.name, p.value, nil}
}

func (p *ParsableString) Get() string {
	if p.err != nil {
		p.errorHandler(p.name, p.err)
	}
	return p.value
}

func (p *ParsableString) Error() error {
	return p.err
}

func (p *ParsableString) Match(re *regexp.Regexp) *ParsableString {
	if !re.MatchString(p.value) {
		p.err = fmt.Errorf("must match regular expression %s", re.String())
	}
	return p
}

func (p *ParsableString) MinLen(min int) *ParsableString {
	if len(p.value) < min {
		p.err = fmt.Errorf("must be at least %d characters long", min)
	}
	return p
}

func (p *ParsableString) MaxLen(max int) *ParsableString {
	if len(p.value) > max {
		p.err = fmt.Errorf("must be at most %d characters long", max)
	}
	return p
}
