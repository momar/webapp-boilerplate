package parsable

type Parsable struct {
	errorHandler func(string, error)
	name         string
	value        string
	fallback     string
	required     bool
}

func (p *Parsable) Default(fallback string) *Parsable {
	p.fallback = fallback
	return p
}

func (p *Parsable) Required() *Parsable {
	p.required = true
	return p
}

func New(errorHandler func(string, error), name string, value string) *Parsable {
	return &Parsable{errorHandler, name, value, "", false}
}
