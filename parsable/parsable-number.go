package parsable

import (
	"errors"
	"fmt"
	"strconv"
)

type ParsableNumber struct {
	errorHandler func(string, error)
	name         string
	value        int64
	err          error
}

func (p *Parsable) AsNumber() *ParsableNumber {
	if p.value == "" && p.required {
		return &ParsableNumber{p.errorHandler, p.name, 0, errors.New("field is required")}
	} else if p.value == "" && p.fallback == "" {
		return &ParsableNumber{p.errorHandler, p.name, 0, nil}
	} else if p.value == "" {
		p.value = p.fallback
	}
	v, err := strconv.ParseInt(p.value, 10, 64)
	return &ParsableNumber{p.errorHandler, p.name, v, err}
}

func (p *ParsableNumber) Get() int64 {
	if p.err != nil {
		p.errorHandler(p.name, p.err)
	}
	return p.value
}

func (p *ParsableNumber) Error() error {
	return p.err
}

func (p *ParsableNumber) Min(min int64) *ParsableNumber {
	if p.value < min {
		p.err = fmt.Errorf("must be at least %d", min)
	}
	return p
}

func (p *ParsableNumber) Max(max int64) *ParsableNumber {
	if p.value > max {
		p.err = fmt.Errorf("must be at most %d", max)
	}
	return p
}
