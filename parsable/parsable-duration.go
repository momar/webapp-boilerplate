package parsable

import (
	"errors"
	"fmt"
	"time"
)

type ParsableDuration struct {
	errorHandler func(string, error)
	name         string
	value        time.Duration
	err          error
}

func (p *Parsable) AsDuration() *ParsableDuration {
	if p.value == "" && p.required {
		return &ParsableDuration{p.errorHandler, p.name, time.Duration(0), errors.New("field is required")}
	} else if p.value == "" && p.fallback == "" {
		return &ParsableDuration{p.errorHandler, p.name, time.Duration(0), nil}
	} else if p.value == "" {
		p.value = p.fallback
	}
	v, err := time.ParseDuration(p.value)
	return &ParsableDuration{p.errorHandler, p.name, v, err}
}

func (p *ParsableDuration) Get() time.Duration {
	if p.err != nil {
		p.errorHandler(p.name, p.err)
	}
	return p.value
}

func (p *ParsableDuration) Error() error {
	return p.err
}

func (p *ParsableDuration) Min(min time.Duration) *ParsableDuration {
	if p.value < min {
		p.err = fmt.Errorf("must be at least %d", min)
	}
	return p
}

func (p *ParsableDuration) Max(max time.Duration) *ParsableDuration {
	if p.value > max {
		p.err = fmt.Errorf("must be at most %d", max)
	}
	return p
}
