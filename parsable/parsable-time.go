package parsable

import (
	"errors"
	"time"
)

type ParsableTime struct {
	errorHandler func(string, error)
	name         string
	value        time.Time
	err          error
}

func (p *Parsable) AsTime(layout string) *ParsableTime {
	if p.value == "" && p.required {
		return &ParsableTime{p.errorHandler, p.name, time.Time{}, errors.New("field is required")}
	} else if p.value == "" && p.fallback == "" {
		return &ParsableTime{p.errorHandler, p.name, time.Time{}, nil}
	} else if p.value == "" {
		p.value = p.fallback
	}
	v, err := time.Parse(layout, p.value)
	return &ParsableTime{p.errorHandler, p.name, v, err}
}

func (p *ParsableTime) Get() time.Time {
	if p.err != nil {
		p.errorHandler(p.name, p.err)
	}
	return p.value
}

func (p *ParsableTime) Error() error {
	return p.err
}

func (p *ParsableTime) NowIfMissing() *ParsableTime {
	if p.value == (time.Time{}) {
		p.value = time.Now()
	}
	return p
}
