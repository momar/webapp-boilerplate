package boilerplate

import (
	"codeberg.org/momar/webapp-boilerplate/env"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"strconv"
	"strings"

	//_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog/log"
	"net/url"
	"regexp"
)

func (b *Builder) WithDatabase(connectionFunc ...func(driverName string, dataSourceName string) error) *Builder {

	// SQLite3

	driverName := "sqlite3"
	dataSourceName := "file:sql?mode=memory&cache=shared&_fk=1"

	if sqliteDatabase := env.Read("SQLITE_DATABASE").AsString().Get(); sqliteDatabase != "" {
		dataSourceName = "file:" + sqliteDatabase + "?_fk=1"
	}

	// MySQL

	if mysqlDatabase := env.Read("MYSQL_DATABASE").AsString().Get(); mysqlDatabase != "" {
		driverName = "mysql"

		var (
			mysqlUser     = env.Read("MYSQL_USER").Default("root").AsString().Get()
			mysqlPassword = env.Read("MYSQL_PASSWORD").AsString().Get()
			mysqlHost     = env.Read("MYSQL_HOST").Default("127.0.0.1").AsString().Get()
			mysqlPort     = env.Read("MYSQL_PORT").Default("3306").AsNumber().Min(1).Max(65535).Get()
			mysqlServer   = strings.SplitN(env.Read("MYSQL_SERVER").AsString().Get(), ":", 2)
		)
		if mysqlPassword == "" && mysqlUser == "root" {
			mysqlPassword = env.Read("MYSQL_ROOT_PASSWORD").AsString().Get()
		}
		mysqlHost, mysqlPort = processServerEnvironment(mysqlHost, mysqlPort, mysqlServer)


		dataSourceName = fmt.Sprintf(
			"%s:%s@tcp(%s:%d)/%s?parseTime=True",
			url.QueryEscape(mysqlUser), url.QueryEscape(mysqlPassword),
			url.QueryEscape(mysqlHost), mysqlPort,
			url.QueryEscape(mysqlDatabase),
		)
	}

	// PostgreSQL

	if postgresDatabase := env.Read("POSTGRES_DB").AsString().Get(); postgresDatabase != "" {
		driverName = "postgres"

		var (
			postgresUser     = env.Read("POSTGRES_USER").Default("postgres").AsString().Get()
			postgresPassword = env.Read("POSTGRES_PASSWORD").AsString().Get()
			postgresHost     = env.Read("POSTGRES_HOST").Default("127.0.0.1").AsString().Get()
			postgresPort     = env.Read("POSTGRES_PORT").Default("5432").AsNumber().Min(1).Max(65535).Get()
			postgresServer   = strings.SplitN(env.Read("POSTGRES_SERVER").AsString().Get(), ":", 2)
			postgresSSLMode  = env.Read("POSTGRES_SSLMODE").Default("disable").AsString().Match(regexp.MustCompile(`^(disable|require|verify-ca|verify-full)$`)).Get()
		)
		postgresHost, postgresPort = processServerEnvironment(postgresHost, postgresPort, postgresServer)

		dataSourceName = fmt.Sprintf(
			"host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
			url.QueryEscape(postgresHost), postgresPort,
			url.QueryEscape(postgresUser), url.QueryEscape(postgresPassword),
			url.QueryEscape(postgresDatabase),
			url.QueryEscape(postgresSSLMode),
		)
	}

	// Apply configuration

	for _, fn := range connectionFunc {
		if err := fn(driverName, dataSourceName); err != nil {
			log.Fatal().Err(err).Msg("database setup failed")
		}
	}

	return b
}

func processServerEnvironment(host string, port int64, server []string) (string, int64) {
	if len(server) > 1 || server[0] != "" {
		if server[0] != "" {
			host = server[0]
		}
		if len(server) > 1 {
			if serverPort, err := strconv.ParseInt(server[1], 10, 64); err == nil && port >= 1 && port <= 65535 {
				port = serverPort
			}
		}
	}
	return host, port
}
