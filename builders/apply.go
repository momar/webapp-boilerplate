package builders

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/json-iterator/go"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
	"reflect"
)

func ApplyContext(c *gin.Context, modelStructPointer interface{}, builderPointer interface{}) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("couldn't read body"))
	}

	err = ApplyJSON(body, modelStructPointer, builderPointer)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
}

func ApplyJSON(data []byte, modelStructPointer interface{}, builderPointer interface{}) error {
	err := jsoniter.Unmarshal(data, modelStructPointer)
	if err != nil {
		return err
	}

	ApplyStruct(modelStructPointer, builderPointer)
	return nil
}

func ApplyStruct(modelStructPointer interface{}, builderPointer interface{}) {
	model := reflect.ValueOf(modelStructPointer).Elem()
	builder := reflect.ValueOf(builderPointer)

	modelType := model.Type()
	fields := make([]string, modelType.NumField())
	for i := 0; i < modelType.NumField(); i++ {
		fields[i] = modelType.Field(i).Name
	}

	for _, field := range fields {
		value := model.FieldByName(field)
		log.Debug().Msgf("Processing field %s (%s)", field, value.String())
		if field == "Edges" {

			valueType := value.Type()
			edges := make([]string, valueType.NumField())
			for i := 0; i < valueType.NumField(); i++ {
				edges[i] = valueType.Field(i).Name
			}

			for _, edge := range edges {
				target := value.FieldByName(edge)
				log.Debug().Msgf("Processing edge %s (%s)", edge, target.String())

				clearer := builder.MethodByName("Clear" + edge)
				if clearer.IsValid() {
					log.Debug().Msgf("Clear%s", edge)
					clearer.Call([]reflect.Value{})
				}
				adder := builder.MethodByName("Add" + edge)
				if adder.IsValid() {
					log.Debug().Msgf("Add%s", edge)
					adder.CallSlice([]reflect.Value{target})
				} else {
					setter := builder.MethodByName("Set" + edge)
					if setter.IsValid() {
						log.Debug().Msgf("Set%s", edge)
						setter.Call([]reflect.Value{target})
					}
				}
			}

			continue
		}
		if value.IsZero() {
			log.Debug().Msgf("Value is zero")
			clearer := builder.MethodByName("Clear" + field)
			if clearer.IsValid() {
				log.Debug().Msgf("Clear%s", field)
				clearer.Call([]reflect.Value{})
				continue
			}
			if !builder.MethodByName("Where").IsValid() {
				// We've got a create builder, so we'll leave the default value
				continue
			}
		}
		setter := builder.MethodByName("Set" + field)
		if setter.IsValid() {
			log.Debug().Msgf("Set%s", field)
			setter.Call([]reflect.Value{value})
			continue
		}
		log.Debug().Msgf("No match for %s", field)
	}
}
