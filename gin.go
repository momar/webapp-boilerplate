package boilerplate

import (
	"codeberg.org/momar/webapp-boilerplate/env"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"strings"
)

func (b0 *Builder) WithGin() *BuilderWithGin {
	b := &BuilderWithGin{*b0, "", nil}

	// Listening Address
	b.ListenAddress = env.Read("HOST").AsString().Get() + ":" +
		env.Read("PORT").Default(If(os.Geteuid() == 0, "80", "8080").(string)).AsString().Get()

	formattedAddr := b.ListenAddress
	if strings.HasPrefix(formattedAddr, ":") {
		formattedAddr = "[::]" + formattedAddr
	}
	formattedAddr = strings.TrimSuffix(formattedAddr, ":80")
	log.Info().Str("address", "http://"+formattedAddr).Msgf("Setting up webserver.")

	// Engine
	b.Engine = gin.New()
	if log.Logger.GetLevel() <= zerolog.DebugLevel {
		b.Engine.Use(gin.Logger())
		// TODO: format log output and route/handler info
	}
	b.Engine.Use(ginCustomRecovery)

	return b
}

func ginCustomRecovery(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			if errCast, ok := err.(error); ok {
				log.Warn().Err(errCast).Str("path", c.FullPath()).Msg("A request panicked!")
			} else {
				log.Warn().Interface("error", err).Str("path", c.FullPath()).Msg("A request panicked!")
			}
			c.Error(fmt.Errorf("server panicked"))
		}
		if len(c.Errors) > 0 {
			if !c.Writer.Written() {
				c.Status(http.StatusInternalServerError)
				c.Header("Content-Type", "text/plain")
			}
			if !c.Writer.Written() || c.Writer.Size() == 0 {
				for _, err := range c.Errors {
					c.Writer.WriteString(err.Error() + "\n")
				}
			}
		}
	}()
	c.Next()
}
