package boilerplate

import (
	"bytes"
	"codeberg.org/momar/webapp-boilerplate/env"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"strings"
)

var logLevel = strings.ToLower(env.Read("LOG_LEVEL").Default("info").AsString().Get())

func (b *Builder) WithLogging() *Builder {
	if strings.ToLower(env.Read("LOG_FORMAT").AsString().Get()) != "json" {
		log.Logger = zerolog.New(zerolog.NewConsoleWriter(func(w *zerolog.ConsoleWriter) {
			w.PartsOrder = []string{
				zerolog.TimestampFieldName,
				zerolog.LevelFieldName,
				zerolog.MessageFieldName,
				zerolog.CallerFieldName,
			}
			w.TimeFormat = "2006-01-02 15:04:05"
			w.FormatCaller = func(i interface{}) string {
				if logLevel != "trace" {
					return ""
				}
				s := fmt.Sprint(i)
				s = "caller=" + s[strings.LastIndexByte(s, '/')+1:]
				if w.NoColor {
					return s
				}
				return fmt.Sprintf("\x1b[%dm%v\x1b[0m", 90, s)
			}
		}))
	}

	log.Logger = log.Logger.Hook(zerolog.HookFunc(func(e *zerolog.Event, level zerolog.Level, message string) {
		*e = *(e.Timestamp())
	}))

	gin.SetMode(gin.ReleaseMode)
	switch logLevel {
	case "trace":
		log.Logger = log.Logger.Level(zerolog.TraceLevel)
		gin.SetMode(gin.DebugMode)
	case "debug":
		log.Logger = log.Logger.Level(zerolog.DebugLevel)
		gin.SetMode(gin.DebugMode)
	case "info":
		log.Logger = log.Logger.Level(zerolog.InfoLevel)
	case "warn":
		log.Logger = log.Logger.Level(zerolog.WarnLevel)
	case "error":
		log.Logger = log.Logger.Level(zerolog.ErrorLevel)
	default:
		log.Fatal().Str("name", "LOG_LEVEL").Err(fmt.Errorf("must be a valid zerolog level (trace|debug|info|warn|error)")).Msg("invalid environment variable")
	}

	gin.DefaultWriter = ginWriter{false, log.Logger}
	gin.DefaultErrorWriter = ginWriter{true, log.Logger}

	if logLevel == "trace" {
		log.Logger = log.Logger.Hook(zerolog.HookFunc(func(e *zerolog.Event, level zerolog.Level, message string) {
			*e = *(e.Caller(4))
		}))
	}

	return b
}

type ginWriter struct{
	err bool
	logger zerolog.Logger
}

func (w ginWriter) Write(p []byte) (n int, err error) {
	length := len(p)
	p = bytes.TrimPrefix(p, []byte("[GIN-debug] "))
	p = bytes.TrimPrefix(p, []byte("[GIN] "))
	p = bytes.TrimSpace(p)
	level := zerolog.DebugLevel
	if w.err {
		level = zerolog.ErrorLevel
	}
	if bytes.HasPrefix(p, []byte("[WARNING] ")) {
		p = bytes.TrimPrefix(p, []byte("[WARNING] "))
		level = zerolog.WarnLevel
		if bytes.HasPrefix(p, []byte("Running in \"debug\" mode.")) {
			return length, nil
		}
	}
	if bytes.HasPrefix(p, []byte("Environment variable PORT is undefined. ")) || bytes.HasPrefix(p, []byte("Listening and serving ")) {
		return length, nil
	}
	lines := bytes.Split(p, []byte("\n"))
	for _, line := range lines {
		w.logger.WithLevel(level).Timestamp().Caller(5).Msgf("%s", line)
	}
	return length, nil
}
