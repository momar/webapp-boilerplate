# Environment Helpers

This is a helper for accessing environment variables:

```go
var (
    mysqlUser     = env.Read("MYSQL_USER").Default("root").AsString().Get()
    mysqlPassword = env.Read("MYSQL_PASSWORD").AsString().Get()
    mysqlHost     = env.Read("MYSQL_HOST").Default("127.0.0.1").AsString().Get()
    mysqlPort     = env.Read("MYSQL_PORT").Default("3306").AsNumber().Min(1).Max(65535).Get()
)
```
