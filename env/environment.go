package env

import (
	"codeberg.org/momar/webapp-boilerplate/parsable"
	"github.com/rs/zerolog/log"
	"os"
)

func Read(name string, mustExist ...bool) *parsable.Parsable {
	value, ok := os.LookupEnv(name)
	if !ok && (len(mustExist) > 0 && mustExist[0]) {
		log.Fatal().Str("name", name).Msg("missing environment variable")
	}
	return parsable.New(func(name string, err error) {
		log.Fatal().Str("name", name).Err(err).Msg("invalid environment variable")
	}, name, value)
}
