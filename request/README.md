# Gin Request Helpers

This is a helper for accessing query arguments, path parameters and
headers in a convenient way:

```go
app.GET("/:id", func(c *gin.Context) {
	id := request.From(c).Param("id").Required().AsUUID().Get()
	start := request.From(c).Query("start").AsTime("2006-01-02").NowIfMissing().Get()
	days := request.From(c).Query("end").Default("90").AsNumber().Min(1).Max(365).Get()
	if c.IsAborted() {
		return
	}
	
	// ...
})
```
