package request

import (
	"codeberg.org/momar/webapp-boilerplate/parsable"
	"fmt"
	"github.com/gin-gonic/gin"
)

func From(c *gin.Context) Request {
	return Request{c}
}

type Request struct {
	c *gin.Context
}

func (r Request) errorHandler(name string, err error) {
	r.c.AbortWithError(400, fmt.Errorf("parameter \"%s\" is invalid: %w", name, err))
}

func (r Request) Param(key string) *parsable.Parsable {
	return parsable.New(r.errorHandler, key, r.c.Param(key))
}

func (r Request) Query(key string) *parsable.Parsable {
	return parsable.New(r.errorHandler, key, r.c.Query(key))
}

func (r Request) Header(key string) *parsable.Parsable {
	return parsable.New(r.errorHandler, key, r.c.GetHeader(key))
}
