package boilerplate

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

type Builder struct {}

type BuilderWithGin struct {
	Builder
	ListenAddress string
	Engine *gin.Engine
}

func CreateApp() *Builder {
	return &Builder{}
}

func (b *BuilderWithGin) Start() {
	log.Fatal().Err(b.Engine.Run(b.ListenAddress)).Msg("server exited")
}
