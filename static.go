package boilerplate

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"strings"
)

func (b *BuilderWithGin) WithStatic(prefix string, fs http.FileSystem, allowIndex bool) *BuilderWithGin {
	prefix = strings.TrimSuffix(prefix, "/")
	handler := http.StripPrefix(prefix + "/", http.FileServer(fs))
	b.Engine.Use(func(c *gin.Context) {
		// Only allow GET & HEAD methods for static files
		if (c.Request.Method != "GET" && c.Request.Method != "HEAD") || (!strings.HasPrefix(c.Request.URL.Path, prefix + "/") && c.Request.URL.Path != prefix) {
			c.Next()
			return
		}

		fsPath := strings.TrimPrefix(c.Request.URL.Path, strings.TrimSuffix(prefix, "/"))
		f, err := fs.Open(fsPath)
		if err == nil {
			if s, err := f.Stat(); err == nil && s.IsDir() {
				// Only do something if an index.html file exists or if allowIndex is true
				indexExists := allowIndex
				if !allowIndex {
					f, err := fs.Open(strings.TrimSuffix(fsPath, "/") + "/index.html")
					if os.IsNotExist(err) {
						c.Next()
						return
					} else if err != nil {
						log.Warn().Err(err).Msg("Static request failed during Open() of index.html!")
						c.AbortWithError(http.StatusForbidden, fmt.Errorf("permission error for file access"))
						return
					}
					s, err := f.Stat()
					if err != nil {
						log.Warn().Err(err).Msg("Static request failed during Stat() of index.html!")
						c.AbortWithError(http.StatusForbidden, fmt.Errorf("permission error for file access"))
						return
					}
					indexExists = !s.IsDir()
				}
				if indexExists {
					// If the user calls a directory without a trailing slash, add one
					if !strings.HasSuffix(c.Request.URL.Path, "/") {
						c.Redirect(http.StatusFound, c.Request.URL.Path+"/")
						return
					}

					// Let our handler serve either the index.html file or the directory index
					handler.ServeHTTP(c.Writer, c.Request)
					return
				}
			} else if err == nil {
				// If the user calls /index.html manually, redirect him to /
				if strings.HasSuffix(c.Request.URL.Path, "/index.html") {
					c.Redirect(http.StatusFound, strings.TrimSuffix(c.Request.URL.Path, "index.html"))
					return
				}

				// We've got a file, so we'll serve it!
				handler.ServeHTTP(c.Writer, c.Request)
				return
			} else {
				log.Warn().Err(err).Msg("Static request failed during Stat()!")
				c.AbortWithError(http.StatusForbidden, fmt.Errorf("permission error for file access"))
				return
			}
		} else if !os.IsNotExist(err) {
			log.Warn().Err(err).Msg("Static request failed during Open()!")
			c.AbortWithError(http.StatusForbidden, fmt.Errorf("permission error for file access"))
			return
		}
		c.Next()
	})
	return b
}
