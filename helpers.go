package boilerplate

import (
	"reflect"
)

func Must(err error) {
	if err != nil {
		panic(err)
	}
}

func If(condition bool, yes interface{}, no interface{}) interface{} {
	if condition {
		return yes
	} else {
		return no
	}
}

func Default(value interface{}, fallback interface{}) interface{} {
	if reflect.ValueOf(value).IsZero() {
		return fallback
	}
	return value
}
