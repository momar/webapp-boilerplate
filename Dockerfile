FROM golang:alpine AS build

RUN apk add --no-cache git build-base ca-certificates
RUN go get github.com/facebook/ent/cmd/entc
RUN go get github.com/markbates/pkger/cmd/pkger

ADD go.mod go.sum /build/
WORKDIR /build/
RUN go mod download -x
ADD . /build/
RUN go generate -x ./...
RUN CGO_ENABLED=1 go build -v -ldflags '-s -w -extldflags "-static"' -o /tmp/server ./cmd/*

FROM scratch
COPY --from=build /etc/ssl /etc/ssl
COPY --from=build /tmp/server /bin/server
EXPOSE 80
ENTRYPOINT ["/bin/server"]
