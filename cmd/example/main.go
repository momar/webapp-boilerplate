package main

import (
	"codeberg.org/momar/webapp-boilerplate"
	"database/sql"
	"github.com/gin-gonic/gin"
	"github.com/markbates/pkger"
	"github.com/rs/zerolog/log"
	"io/ioutil"
)

//go:generate pkger

var db *sql.DB

func main() {
	// Open static files
	staticFiles, err := pkger.Open("/")
	boilerplate.Must(err)

	app := boilerplate.CreateApp().
		WithLogging().
		WithDatabase(func(driverName string, dataSourceName string) (err error) {
			// Connect to the database
			log.Info().Str("driver", driverName).Str("dataSource", dataSourceName).Msg("Connecting to database.")
			db, err = sql.Open(driverName, dataSourceName)
			return
		}, func(string, string) (err error) {
			// Initialize database schema
			_, err = db.Exec("CREATE TABLE IF NOT EXISTS data (key VARCHAR(255) PRIMARY KEY NOT NULL, value VARCHAR(255) NOT NULL)")
			return
		}).
		WithGin().
		WithStatic("/", staticFiles, true)

	app.Engine.GET("/api/:key", func(c *gin.Context) {
		rows, err := db.Query("SELECT value FROM data WHERE key = ?", c.Param("key"))
		boilerplate.Must(err)
		defer rows.Close()
		if rows.Next() {
			value := ""
			boilerplate.Must(rows.Scan(&value))
			c.String(200, value)
		} else {
			c.String(404, "missing data")
		}
	})

	app.Engine.PUT("/api/:key", func(c *gin.Context) {
		data, err := ioutil.ReadAll(c.Request.Body)
		boilerplate.Must(err)

		result, err := db.Exec("UPDATE data SET value = ? WHERE key = ?", string(data), c.Param("key"))
		boilerplate.Must(err)
		if i, _ := result.RowsAffected(); i < 1 {
			_, err = db.Exec("INSERT INTO data (key, value) VALUES (?, ?)", c.Param("key"), string(data))
			boilerplate.Must(err)
		}

		c.String(200, "ok")
	})

	app.Start()
}
